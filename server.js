var express = require('express');
var app = express();
var bodyParser = require('body-parser'); //Se utiliza para parsear el json
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

//Variables para gestionar el acceso a MongoDB con mLab
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechupsj/collections/";
var proyMlabURL = "https://api.mlab.com/api/1/databases/proyectopsj/collections/";
var mLabAPIKey = "apiKey=RIRPGgUWpSCGy3HguQXwGddMOliniFZr";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1',
  function(req, res) {
    console.log("GET /apitechu/v1");
    res.send(
      {
        "msg" : "Bienvenido a la API de TechU"
      }
    );
  }
);

app.get('/apitechu/v1/users',
  function(req, res) {
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root: __dirname});
    //res.sendfile('./usuarios.json');//Deprecated
  }
);

app.get('/apitechu/v2/users',
  function(req, res) {
    console.log("GET /apitechu/v2/users");

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Lista de clientes");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
          }
          res.send(response);
      }
    )
  }
);

app.get('/apitechu/v2/users/:id',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Consulta cliente");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

app.get('/apitechu/v2/users/:id/accounts',
  function(req, res) {
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid" : ' + id + '}';

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Consulta cuentas cliente");

    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuentas."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);



app.post('/apitechu/v2/login',
  function(req, res) {
    console.log("POST /apitechu/v2/login");

    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email" : "' + email + '", "password" : "' + password + '"}';
    console.log(query);

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Consulta cliente");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
             query = 'q={"id" : ' + body[0].id +'}';
             var putBody = '{"$set":{"logged":true}}';

             httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT){
                  console.log("Usuario logado");
                }
             )
           } else {
             response = {
               "msg" : "Usuario y/o password incorrecto."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

app.post('/apitechu/v2/logout',
  function(req, res) {
    console.log("POST /apitechu/v2/logout");

    var id = req.body.id;
    var query = 'q={"id" : ' + id + '}';
    console.log(query);

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Consulta cliente");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
             query = 'q={"id" : ' + body[0].id +'}';
             var putBody = '{"$unset":{"logged":""}}';

             httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT){
                  console.log("Usuario deslogado");
                }
             )
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

/* //////////////////////Inicio Código Login y Logout del profesor

app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;

   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

*////////////////////////Fin Código Login y Logout del profesor

app.post('/apitechu/v1/users',
  function(req, res) {
    console.log("POST /apitechu/v1/users");

    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("country es " + req.body.country);

    var newUser ={
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);


    //res.send(users); //SIN PERSISTENCIA

    res.send(
      {
      "msg" : "Usuario añadido con éxito"
      }
    );
  }
)

app.delete('/apitechu/v1/users/:id',
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1 , 1);
    writeUserDataToFile(users);

    res.send(
      {
      "msg" : "Usuario borrado con éxito"
      }
    );

  }
)

app.post('/apitechu/v1/monstruo/:p1/:p2',
function (req, res) {
  console.log("Parámetros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);
  }
)

//  ESTA VERSIÓN SE SUSTITUYE POR LA DEL PROYECTO MÁS ADELANTE
// app.post('/apitechu/v1/login',
//   function(req, res) {
//     console.log("POST /apitechu/v1/login");
//
//     console.log("email es " + req.body.email);
//     console.log("password es " + req.body.password);
//
//     var users = require('./usuarios.json');
//     var correcto = false;
//     for (user of users) {
//       mail = user.email;
//       pass = user.password;
//       if (mail == req.body.email) {
//         console.log("El mail válido es " + mail);
//         if (pass == req.body.password) {
//           res.send(
//             {
//             "mensaje" : "Login correcto",
//             "idUsuario" : user.id
//             }
//           );
//           correcto = true;
//           user.logged=true;
//           console.log(user);
//           break;
//         }
//       }
//     }
//     if (!correcto){
//       res.send(
//         {
//         "mensaje" : "Login incorrecto"
//         }
//       );
//     }
//     writeUserDataToFile(users);
//
//     //res.send(users); //SIN PERSISTENCIA
//
//   }
// )


// ESTA VERSIÓN SE SUSTITUYE POR LA DEL PROYECTO MÁS ADELANTE
// app.post('/apitechu/v1/logout',
//   function(req, res) {
//     console.log("POST /apitechu/v1/logout");
//
//     console.log("id es " + req.body.id);
//
//     var users = require('./usuarios.json');
//     var correcto = false;
//     for (user of users) {
//       id = user.id;
//       if (id == req.body.id) {
//         console.log("El id válido es " + id);
//         if (user.logged == true) {
//           res.send(
//             {
//             "mensaje" : "Login correcto",
//             "idUsuario" : user.id
//             }
//           );
//           correcto = true;
//           delete user.logged;
//           break;
//         }
//       }
//     }
//     if (!correcto){
//       res.send(
//         {
//         "mensaje" : "Login incorrecto"
//         }
//       );
//     }
//     writeUserDataToFile(users);
//
//     //res.send(users); //SIN PERSISTENCIA
//
//   }
// )

function writeUserDataToFile(data) {
var fs = require('fs'); //Se utiliza para trabajar con ficheros
var jsonUserData = JSON.stringify(data);

fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
      if (err)
      {
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
    }
  )
}

///////////////////////// Mi proyecto

// Devuelve el listado de todas las cuentas de la BD
app.get('/apitechu/v1/cuentas',
  function(req, res) {
    console.log("GET /apitechu/v1/cuentas");

    httpClient = requestJson.createClient(proyMlabURL);
    console.log("Lista de cuentas");

    httpClient.get("cuenta?" + mLabAPIKey,
      function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error obteniendo cuentas."
          }
          res.send(response);
      }
    )
  }
);

// Devuelve la información de una cuenta
app.get('/apitechu/v1/cuentas/:iban',
  function(req, res) {
    console.log("GET /apitechu/v1/cuentas/:iban");

    var iban = req.params.iban;
    var query = 'q={"iban" : "' + iban + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Cuenta no encontrada."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

// Crea un nuevo movimiento en una cuenta
app.post('/apitechu/v1/cuentas/movimientos',
  function(req, res) {
    console.log("POST /apitechu/v1/cuentas/movimientos");

    var iban = req.body.iban;
    var tipo = req.body.tipo;
    var importe = parseInt(req.body.importe);
    var detalle = req.body.detalle;
    var fecha = new Date();
    var query = 'q={"iban" : "' + iban + '"}';
    console.log(importe);
    console.log(typeof importe);

    httpClient = requestJson.createClient(proyMlabURL);
    console.log("Consulta cliente");

    httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             var saldo = body[0].saldo;
             var operacionok = true;
             console.log("El tipo es " + tipo);
             switch (tipo){
                case "A":
                  nuevosaldo = saldo + importe;
                  break;
                case "C":
                  if (importe > saldo){
                    operacionok = false;
                    response = {
                      "msg" : "Saldo insuficiente."
                    }
                  }  else {
                      nuevosaldo = saldo - importe;
                    }
                  break;
             }
             if (operacionok) {
               query = 'q={"iban" : "' + body[0].iban +'"}';
               var putBody = '{"$push": {"movimiento": { "importe" : ' + importe + ', "divisa" : "EUR", "tipo" : "' + tipo + '", "detalle" : "' + detalle + '", "fecha" : "' + fecha + '"}},"$set": {"saldo" : ' + nuevosaldo + '}}';

               httpClient.put("cuenta?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT){
                    console.log("Movimiento realizado");
                  }
               )
               response = {"msg" : "Movimiento realizado sobre la cuenta " + iban + " por " + importe + " EUR"};
             }
           } else {
             response = {
               "msg" : "Cuenta no encontrada."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

// Devuelve la información de un cliente
app.get('/apitechu/v1/clientes',
  function(req, res) {
    console.log("GET /apitechu/v1/clientes");

    var nif = req.headers.nif;
    var query = 'q={"nif" : "' + nif + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cliente."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Cliente no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

// Crea un nuevo cliente
app.post('/apitechu/v1/clientes',
  function(req, res) {
    console.log("POST /apitechu/v1/clientes");

    var sha1 = require('sha1');
    var putBody ={
      "nif" : req.body.nif,
      "nombre" : req.body.nombre,
      "apellidos" : req.body.apellidos,
      "email" : req.body.email,
      "password" : sha1(req.body.password)
    };

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.post("cliente?" + mLabAPIKey, putBody,
    function(errPUT, resMLabPUT, bodyPUT){
         console.log("Cliente creado");
         response = {
           "msg" : "Cliente creado."
         };
         res.send(response);
       }
    )
  }
);

// Login de un cliente
app.post('/apitechu/v1/login',
  function(req, res) {
    console.log("POST /apitechu/v1/login");

    var sha1 = require('sha1');
    var nif = req.body.nif;
    var password = sha1(req.body.password);
    var query = 'q={"nif" : "' + nif + '", "password" : "' + password + '"}';
    console.log(query);

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
             query = 'q={"_id" : ' + JSON.stringify(body[0]._id) +'}';
             var putBody = '{"$set":{"logged":true}}';

             httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT){
                  console.log("Usuario logado");
                }
             )
           } else {
             response = {
               "msg" : "Usuario o clave incorrecta."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

// Logout de un cliente
app.post('/apitechu/v1/logout',
  function(req, res) {
    console.log("POST /apitechu/v1/logout");

    var nif = req.body.nif;
    var query = 'q={"nif" : "' + nif + '"}';
    console.log(query);

    httpClient = requestJson.createClient(proyMlabURL);
    console.log("Consulta cliente");

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response ={
               "msg" : "Usuario desconectado."
             };
             query = 'q={"nif" : "' + body[0].nif +'"}';
             var putBody = '{"$unset":{"logged":""}}';

             httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT){
                  console.log("Usuario deslogado");
                }
             )
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )

  }
);

// Olvidó su clave. Envío de código de reactivación
app.post('/apitechu/v1/clave',
  function(req, res) {
    console.log("POST /apitechu/v1/clave");

    var nif = req.body.nif;
    var query = 'q={"nif" : "' + nif + '"}';

    httpClient = requestJson.createClient(proyMlabURL);

    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
       if (body.length > 0) {
         var email = body[0].email;
         var nodemailer = require('nodemailer');

         var codigo = Math.round(Math.random() * 900000 + 100000);
         var feccodigo = new Date();
         console.log(feccodigo);


         query = 'q={"_id" : ' + JSON.stringify(body[0]._id) +'}';
         var putBody = '{"$set":{"codigoreset":' + codigo + ', "expirareset":"' + feccodigo + '"}}';
         console.log(putBody);

         httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT){
              console.log("Código creado");
            }
         )

         var transporter = nodemailer.createTransport({
           service: 'gmail',
           auth: {
             user: 'proyectotechu@gmail.com',
             pass: 'dcqaymlmP0'
           }
         });

         var mailOptions = {
           from: 'proyectotechu@gmail.com',
           to: email,
           subject: 'Reseteo de password',
           text: 'El código para resetear su password es el siguiente: ' + codigo + '. Tiene validez durante 5 minutos.'
         };

         transporter.sendMail(mailOptions, function(error, info){
           if (error) {
             console.log(error);
           } else {
             console.log('Email sent: ' + info.response);
           }
         });
        }
         res.send(
            {
               "msg" : "Se enviará un correo a su dirección email."
            }
          );
       }
    )

  }
);

// Reactivación clave
app.post('/apitechu/v1/clave/reactivacion',
  function(req, res) {
    console.log("POST /apitechu/v1/clave/reactivacion");

    var sha1 = require('sha1');

    var nif = req.body.nif;
    var codigointro = req.body.codigo;
    var fecintro = new Date();
    var query = 'q={"nif" : "' + nif + '"}';

    console.log(query);

    httpClient = requestJson.createClient(proyMlabURL);

    //En primer lugar recupera la información del cliente
    httpClient.get("cliente?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {

             var codigoreset = body[0].codigoreset;
             var expirareset = body[0].expirareset;
             if (codigointro != codigoreset) {
               response = {
                 "msg" : "Código incorrecto."
               };
             } else {
               if ((Date.parse(fecintro) - Date.parse(expirareset)) > 300000) {
                 response = {
                   "msg" : "El código ha expirado."
                 };
               } else {
                 // Código de reactivación correcto y válido
                 var password =  sha1(req.body.password)
                 var putBody = '{"$set":{"password": "' + password + '", "logged":true}, "$unset":{"codigoreset":"", "expirareset":""}}';

                 httpClient.put("cliente?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(errPUT, resMLabPUT, bodyPUT){
                      console.log("Password reactivada");
                    }
                 )
                response = {
                  "msg" : "Password reactivada."
                };
               }
             }
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);
